import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import ACCESS_ENUM from "@/access/accessEnum";
import NoAuthView from "@/views/NoAuthView.vue";

export const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: '浏览题目',
        component: HomeView
    },
    {
        path: "/noAuth",
        name: "无权限",
        component: NoAuthView,
        meta: {
            hideInMenu: true,
        },
    },
    {
      path: "/hide",
      name: "管理员可见",
      component: HomeView,
      meta: {
          access: ACCESS_ENUM.ADMIN,
      },
    },
    {
        path: '/about',
        name: '关于我的',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    }
]